CREATE TABLE tb_performer
(
    id INT(10) NOT NULL UNIQUE,
    name VARCHAR(20),
    dateOfBirth VARCHAR(20)
); 

CREATE TABLE tb_music
(
    id INT(10) NOT NULL UNIQUE,
    songTitle VARCHAR(20)
);

CREATE TABLE tb_song
(
    id INT(10) NOT NULL UNIQUE,
    text VARCHAR(2048)
);  



INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (23, "Britney Jean Spears ", 1981);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (44, "Billie Eilish", 2001);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (32, "Britney Jean Spears ", 1981);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (24, "Monatic", 1986);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (25, "Alla Pugachova", 1949);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (26, "Michael Joseph Jackson", 1958);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (27, "Philip Kirkorov", 1967);

INSERT INTO tb_performer (id,name,dateOfBirth)
VALUES (28, "Alyosha", 1986);



INSERT INTO tb_music (id,songTitle)
VALUES (65, "Do you wanna come over");

INSERT INTO tb_music (id,songTitle)
VALUES (56, "when I was older");


INSERT INTO tb_music (id,songTitle)
VALUES (59, "Toxic");

INSERT INTO tb_music (id,songTitle)
VALUES (84, "gjklsdjklgjdslkfgjldskjfglksdjlk");

INSERT INTO tb_music (id,songTitle)
VALUES (66, "Spinning");

INSERT INTO tb_music (id,songTitle)
VALUES (67, "Every night and every day");

INSERT INTO tb_music (id,songTitle)
VALUES (68, "The way you love me");

INSERT INTO tb_music (id,songTitle)
VALUES (69, "Viva!");

INSERT INTO tb_music (id,songTitle)
VALUES (70, "Sweet People!");




INSERT INTO tb_song (id,text)
VALUES (5, "(Uh huh)
What?
Say you feel alone, that your day was the baddest
Tellin' me you can’t sleep because of your mattress
Do you wanna come over? (Yeah)
Do you wanna come over? (Yeah)
Let me rub your back, you can set limitations
Honey, if you want we can get out frustrations
Do you wanna come over? (Just let me know)
Do you wanna come over? (Yeah)

Cause nobody should be alone if they don't have to be
Nobody should be alone if they don't have to be
Nobody should be alone if they don’t have to be
Should be alone if they don't have to be
So...

Whatever you want (Whatever you want)
Whatever you need (Whatever you need)
I'll do it (uh huh)
I'll do it (uh huh)
Whatever you want (Whatever you want)
Whatever you need (Whatever you need)
I'll do it (uh huh)
I'll do it (uh huh)
Do you wanna come over?

I could get into that kissin' and touchin'
Or we could be good and do next to nothin’
Do you wanna come over? (Yeah)
Do you wanna come over? (Yeah, let me know)
Cause all I want is what you want
And all you want is me
Yeah, all you want is what I want
And all I want is you (Yeah)

Cause nobody should be alone if they don’t have to be
Nobody should be alone if they don't have to be
Nobody should be alone if they don’t have to be
Should be alone if they don't have to be
So...

Whatever you want (Whatever you want)
Whatever you need (Whatever you need)
I'll do it (uh huh)
I'll do it (uh huh)
Whatever you want (Whatever you want)
Whatever you need (Whatever you need)
I’ll do it (uh huh)
I'll do it (uh huh)
Do you wanna come over?
Do you want to come over, come over?

Cause all I want is what you I want
And all you want is me
Yeah, all you want is what I want
And all I want is you
Just come over

Whatever you want (whatever you want)
Whatever you need (whatever you need)
I'll do it (uh huh)
I'll do it (uh huh)
Whatever you want (whatever you want)
Whatever you need (whatever you need)
I'll do it (uh huh)
I'll do it (uh huh)
Do you wanna come over?
Do you want to come over? (uh huh)
Come over? Uh huh
Do you want to come over? (uh huh)
Come over? (uh huh)
Do you wanna come over?
");



INSERT INTO tb_song (id,text)
VALUES (6, " When I was older
I was a sailor on an open sea
But now I'm underwater
And my skin is paler than it should ever be

I'm on my back again
Dreaming of a time and place
Where you and I remain the best of friends
Even after all this ends
Can we pretend?
I'm on my, I'm on my back again
It's seeming more and more
Like all we ever do
is see how far it bends
Before it breaks in half and then
We bend it back again

Guess I got caught in the middle of it
Yes I've been taught, got a little of it
In my blood, in my blood
Memories burn like a forest fire
Heavy rain turns any funeral pyre to mud
In the flood

When I was older
I was a sailor on an open sea
But now I'm underwater
And my skin is paler than it should ever be

I'm watching movies back to back in black and white, I never
Seen anybody do it like I do it any better
Been goin' over you, I'm overdue for new endeavors
Nobody lonely like I'm lonely and I don't know whether
You'd really like it in the limelight
You'd sympathize with all the bad guys
I'm still a victim in my own right
But I'm the villain in my own eyes, yeah

When I was older
I was a sailor on an open sea"
);

INSERT INTO tb_song (id,text)
VALUES (7, " Baby, can't you see
I'm calling
A guy like you should wear a warning
It's dangerous
I'm falling
There's no escape
I can't wait
I need a hit
Baby, give me it
You're dangerous
I'm loving it
Too high
Can't come down
Losin' my head
Spinnin' 'round and 'round
Do you feel me now?
With the taste of your lips
I'm on a ride
You're toxic I'm slippin' under (Ohh Ohh)
With a taste of the poison paradise
I'm addicted to you
Don't you know that you're toxic?
And I love what you do
Don't you know that you're toxic?
It's getting late
To give you up
I took a sip
From my devil's cup
Slowly, it's taking over me
Too high
Can't come down
It's in the air and it's all around
Can you feel me now?
With the taste of your lips
I'm on a ride
You're toxic I'm slippin' under
With the taste of the poison paradise
I'm addicted to you
Don't you know that you're toxic?
And I love what you do
Don't you know that you're toxic?
Don't you know that you're toxic?
Taste of your lips I'm on a ride
You're toxic I'm slippin' under
With a the taste of a poison paradise
I'm addicted to you
Don't you know that you're toxic?
With the taste of your lips
I'm on a ride
You're toxic I'm slippin' under (Toxic)
With the taste of the poison paradise
I'm addicted to you
Don't you know that you're toxic?
Intoxicate me now
With your lovin' now
I think I'm ready now
(I think I'm ready now)
Intoxicate me now
With your lovin' now
I think I'm ready now ");


INSERT INTO tb_song (id,text)
VALUES (8, "You’re a summer, girl.
You know, you’re summer, girl…
There’s someone in the air in million ways,
What they say, but I just cannot see.
There’s someone in the game that people plays,
What they say, but I just cannot see.
There’s someone in the stars the magic place,
What they say, but I just cannot see,
‘Cause you are in my eyes.

Oh, my endorphins are here,
My fantasy’s dear,
I'm overwhelmed, I held a fear,
Have I become an eyesore?
My endorphins are here,
My fantasy’s dear,
Hey, I’m sorry for interfering,
Have I become an eyesore?

Uuh, it turned out,
You beautify the world, you spin.
You beautify the world, you spin.
You beautify the world, you spin,
You spin, you spin, oh, I’m in.

Spin my head, oh, you’re causing a riot,
Damn, girl you’re causing a riot, hey.
Spin my head, oh, you’re causing a riot,
Damn, you’re hot, give it to me.

Heads are spinning,
Heads are spinning…

There’s someone in the air in million ways,
What they say, but I just cannot see.
There’s someone in the game that people plays,
What they say, but I just cannot see.
There’s someone in the stars the magic place,
What they say, but I just cannot see,
‘Cause you are in my eyes.

Oh, my endorphins are here,
My fantasy’s dear,
I'm overwhelmed, I held a fear,
Have I become an eyesore?
My endorphins are here,
My fantasy’s dear,
Hey, I’m sorry for interfering,
Have I become an eyesore?"
);

INSERT INTO tb_song (id,text)
VALUES (9, "Every night and every day
I will turn to you and say
Will the beauty come our way?
Is it time for us to play
And the time to start believing?
Do we know what we are feeling?
Do we know?

Every night and every day
You'll hear me say
Every night and every day
(We gotta live, we gotta live)

Time for peace, no time for war
What we should be asking for?
Speak up loudly rich and poor
Time for peace, no time for war
Can we live our lives with fire?
Bitter love and sweet desire
We gotta live
Every night and every day
We gotta live
Every night and every day

Every night and every day
I will turn to you and say
Will the beauty come our way?
Is it time for us to play
And the time to start believing?
Do we know what we are feeling?
Do we know?

Every night and every day
You'll hear me say
Every night and every day
(We gotta live, we gotta live)"
);

INSERT INTO tb_song (id,text)
VALUES (10, "I was alone in the dark when I met ya
You took my hand and you told me you loved me

I was alone, there was no love in my life
(I was alone, there was no love in my life)
I was afraid of life and you came in time
(I was afraid of life and you came in time)
You took my hand and we kissed in the moonlight
(You took my hand and we kissed in the moonlight)

I like the way how you're holdin' me
It doesn't matter how you're holdin' me
I like the way how you're lovin me'
It doesn't matter how you are lovin' me
I like the way how you're touchin' me
It doesn't matter how you are touchin' me
I like the way how you're kissin' me
It doesn't matter how you are kissin' me
You'll seeeee...

... it won't be long 'till we make vows, I bet ya

I thank the Heavens above that I met ya

I was alone, there was no love in my life
(I was alone, there was no love in my life)
I was afraid of life and you came in time
(I was afraid of life and you came in time)
You took my hand and we kissed in the moonlight
(You took my hand and we kissed in the moonlight)

I like the way how you're lovin' me
It doesn't matter how you are holdin' me
I like the way how you're holdin' me
It doesn't matter how you re lovin' me
I like the way how you're touchin' me
It doesn't matter how you are touchin' me
I like the way how you're kissin' me
It doesn't matter how you are kissin' me

The world's a better place
'Cause you came in time,
You took away the rain and brought the sunshine,
I was afraid 'cause I was hurt the last time,

I like the way how you're holdin' me,
It doesn't matter how you are holdin' me,
I like the way how you're lovin' me,
It doesn't matter how you are lovin' me,
I like the way how you're touchin' me,
It doesn't matter how you are touchin' me,
I like the way how you're kissin' me,
It doesn't matter how you are kissin' me,
You'll seeeee."
);

INSERT INTO tb_song (id,text)
VALUES (11, "She's all
You'll ever dream to find,
On her stage
She sings her story.
Pain and hurt
Will steal her heart alight,
Like a queen
In all her glory.

And when she cries
Diva is an angel,
When she laughs, she's a devil,
She is all beauty and love.

Refrain: Viva la diva
Viva Victoria
Aphrodita
Viva la diva
Viva Victoria
Cleopatra

Silent tears
Drop from these eyes tonight,
Tears of prayer
For all those aching hearts

And when she cries
Diva is an angel,
When she laughs, she's a devil,
She is all beauty and love.

Refrain (2)
Hey oh ahh, Haahaaa,
Ahhhh, Ahhhh, Oooooooh.

Viva! Viva! Viva! Viva!"
);

INSERT INTO tb_song (id,text)
VALUES (12, "The lyrics of Sweet People
Oh, sweet people
What have we done?
Tell me what is happening?
For all that we've built
Tumbles and is gone

Oh, sweet people
Have you no love for mankind?
Must you go on killing
Just to pass the time.

The message is so true
The end is really near
All these feelings take me down
It steals the things so dear
Yes, the message is so real.

Don't turn all the earth to stone
Because, because, because
This is your home
Oh, sweet people
what about our children?

In theaters and video games
They watch what we send to ruin
Oh, sweet people
What senseless game
Have we all been playing?
No one but you to blame?

The message is so true
The end is really near
All these feelings take me down
It steals the things so dear
Yes, the message is so real.
Don't turn all the earth to stone
Because, because, because
This is your home
This is our home."
);

 
 
 CREATE TABLE tb_marge
 (
  id int(10) NOT NULL UNIQUE,
  idPerformer int(10) NOT NULL,
  idMusic int(10) NOT NULL,
  idSong int(10) NOT NULL,
  CONSTRAINT marge_unique UNIQUE (idPerformer, idMusic, idSong),
  FOREIGN KEY (idPerformer) REFERENCES tb_performer(id),
  FOREIGN KEY (idMusic) REFERENCES tb_music(id),
  FOREIGN KEY (idSong) REFERENCES tb_song(id)
  );

  
  
INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (77, 23, 65, 5);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (30, 44, 56, 6);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (3, 32, 59, 7);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (75, 23, 84, 5);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (71, 24, 66, 8);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (72, 25, 67, 9);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (73, 26, 68, 10);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (74, 27, 69, 11);

INSERT INTO tb_marge (id, idPerformer, idMusic, idSong)
VALUES (76, 28, 70, 12);

SELECT m.idPerformer, p.name, p.dateOfBirth, mu.songTitle, s.text FROM tb_marge AS m INNER JOIN tb_performer AS p ON m.idPerformer= p.id INNER JOIN tb_music AS mu ON m.idMusic= mu.id INNER JOIN tb_song AS s ON m.idSong= s.id  ORDER BY dateOfBirth DESC;