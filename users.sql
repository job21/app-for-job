    Создать таблицу-
 
 CREATE TABLE users
(
    Id INT,
    FirstName NVARCHAR(20),
    LastName NVARCHAR(20)
); 

       Добавить данные в таблицу-

INSERT INTO users (Id, FirstName, LastName) 
VALUES (1, "Marina", "Khlevitska");

INSERT INTO users (Id, FirstName, LastName) 
VALUES (2, "Vasily", "Khlevitsky" )

INSERT INTO users (Id, FirstName, LastName) 
VALUES (3, "Sergey", "Tkachenko")

INSERT INTO users (Id, FirstName, LastName) 
VALUES (4, "Sergey", "Zgama")

       Получить данные из выбранной таблицы-
	   
SELECT * FROM users	 

       Сделать выборку по имени "Sergey"
	   
SELECT Id, FirstName, LastName FROM users WHERE  FirstName= ("Sergey")	   
