CREATE TABLE tb_work
(
    id INT(10) NOT NULL UNIQUE,
    name VARCHAR(20),
    location VARCHAR(20)
); 

CREATE TABLE  tb_users (
    id INT(10) NOT NULL UNIQUE,
    lastName varchar(255) NOT NULL,
    firstName varchar(255),
    workId INT(10) REFERENCES tb_work(id)
);

INSERT INTO tb_work (id,name,location)
VALUES (1, "Home", "Krivoy Rog");

INSERT INTO tb_work (id, name, location)
VALUES (2, "Volkswagen", "Krivoy Rog");

INSERT INTO tb_work (id, name, location)
VALUES (3, "Fortune", "Skadovsk");

INSERT INTO tb_work (id, name, location)
VALUES (4, "At home", "Kropyvnytsky");

INSERT INTO tb_work (id, name, location)
VALUES (5, "Mac Donalds", "Nikolaev");


INSERT INTO  tb_users (id, firstName, lastName, workId) 
VALUES (11, "Marina", "Khlevitska", 1);

INSERT INTO  tb_users (id, firstName, lastName, workId) 
VALUES (12, "Vasily", "Khlevitsky", 2 );

INSERT INTO  tb_users (id, firstName, lastName, workId) 
VALUES (13, "Sergey", "Tkachenko", 3);

INSERT INTO  tb_users (id, firstName, lastName, workId)
VALUES (14, "Sergey", "Zgama", 4);

INSERT INTO  tb_users (id, firstName, lastName, workId)
VALUES (15, "Sergey", "Petrenko", 5);

SELECT u.id, u.firstName, u.lastName, w.name, w.location FROM tb_users AS u INNER JOIN tb_work AS w ON u.workId = w.Id WHERE  firstName="Sergey";